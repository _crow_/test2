import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Test1 {
    public WebDriver driver; //TODO не уверенна можно ли так делать с переменными (в классах новые не создаю)
    public WebElement element;
    public boolean result;

    @BeforeClass
    void before(){
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(priority = 1)
    void LoadCheck(){
        driver.get("https://bookclub.ua/ru/");
        element = driver.findElement(By.xpath("//img[@alt='Клуб Семейного Досуга']"));
        result=element.isDisplayed(); //TODO вынести отдельно? этот кусочек повторяется, кроме выводимого сообщения
        Assert.assertTrue(result,"Logo is a not displayed");
    }

    @Test(priority = 2)
    void CartIsEmpty(){
        element = driver.findElement(By.xpath("//span/b[text()=0]"));
        result=element.isDisplayed();
        Assert.assertTrue(result,"Cart is not empty");
    }

    @Test(priority = 3)
    void SearchOnTheSite(){
        element=driver.findElement(By.name("search"));
        element.sendKeys("Стивен Кинг");
        element.sendKeys(Keys.ENTER);
    }

    @Test(priority = 4)
    void SearchBook(){
        List<WebElement> books = driver.findElements(By.xpath("//a"));
        for (WebElement e:books) {
            if(e.getText().contains("Коли впаде темрява")){
                e.click();
                break;
            }
        }
    }

    @Test(priority = 5)
    void ClickBuyButton(){
        element= driver.findElement(By.xpath("//a/div/img"));
        element.click();
    }

    @Test(priority = 6)
    void ItemInCart(){
        element = driver.findElement(By.xpath("//span/b[text()=1]"));
        result=element.isDisplayed();
        Assert.assertTrue(result,"Cart is empty");
    }

    @AfterClass
    void postCondition(){
        driver.quit();
    }

}
